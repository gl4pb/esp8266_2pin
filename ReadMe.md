ESP8266 2pin breakout module
============================

This board provides a convenient way to use the ESP-01 and related 6 pin breakout boards.

It provides:
* Programming I/O header for direct connection to an FTDI interface module
* Power input with reglated power for the ESP-01 module
* Switches for:
  * Programming (SPST)
  * Reset (momentary push)
* Header access for GPIO 00 & 02 exposed by the ESP-01

